use std::{env, vec};
use std::fmt::Error;
use std::fs;

#[derive(Debug, Clone, Copy)]
struct ToRange {
    source: u64,
    destination: u64,
    length: u64
}

impl ToRange {
    fn is_contained( self, value: u64 ) -> Result<bool, Error> {
        if value >= self.source && value < (self.source + self.length) {
            return Ok(true);
        }

        Err(Error)
    }

    fn get_destination_value( self, source: u64 ) -> Result<u64, Error> {
        if source >= self.source && source < (self.source + self.length) {
            return Ok( self.destination + (source - self.source) );
        }

        Err(Error)
    }

    fn get_source_value( self, destination: u64 ) -> Result<u64, Error> {
        if destination >= self.destination && destination < (self.destination + self.length) {
            return Ok( self.source + (destination - self.destination) );
        }

        Err(Error)
    }
}

#[derive(Debug, Clone)]
struct AlmanacRanges {
    seed_to_soil_ranges: Vec<ToRange>,
    soil_to_fertilizer_ranges: Vec<ToRange>,
    fertilizer_to_water_ranges: Vec<ToRange>,
    water_to_light_ranges: Vec<ToRange>,
    light_to_temperature_ranges: Vec<ToRange>,
    temperature_to_humidity_ranges: Vec<ToRange>,
    humidity_to_location_ranges: Vec<ToRange>
}

impl AlmanacRanges {
    fn get_seed_location_value( self, seed: u64 ) -> u64 {
        let mut found_soil: u64 = seed;
        for range in self.seed_to_soil_ranges {
            match range.get_destination_value(seed) {
                Ok(soil) => { found_soil = soil; break; }
                Err(_) => { }
            }
        }

        let mut found_fertilizer: u64 = found_soil;
        for range in self.soil_to_fertilizer_ranges {
            match range.get_destination_value(found_soil) {
                Ok(fertilizer) => { found_fertilizer = fertilizer; break; }
                Err(_) => { }
            }
        }

        let mut found_water: u64 = found_fertilizer;
        for range in self.fertilizer_to_water_ranges {
            match range.get_destination_value(found_fertilizer) {
                Ok(water) => { found_water = water; break; }
                Err(_) => { }
            }
        }

        let mut found_light: u64 = found_water;
        for range in self.water_to_light_ranges {
            match range.get_destination_value(found_water) {
                Ok(light) => { found_light = light; break; }
                Err(_) => { }
            }
        }

        let mut found_temperature: u64 = found_light;
        for range in self.light_to_temperature_ranges {
            match range.get_destination_value(found_light) {
                Ok(temperature) => { found_temperature = temperature; break; }
                Err(_) => { }
            }
        }

        let mut found_humidity: u64 = found_temperature;
        for range in self.temperature_to_humidity_ranges {
            match range.get_destination_value(found_temperature) {
                Ok(humidity) => { found_humidity = humidity; break; }
                Err(_) => { }
            }
        }

        let mut found_location: u64 = found_humidity;
        for range in self.humidity_to_location_ranges {
            match range.get_destination_value(found_humidity) {
                Ok(location) => { found_location = location; break; }
                Err(_) => { }
            }
        }

        return found_location;
    }

    fn get_location_seed_value( self, location: u64 ) -> u64 {
        let mut found_humidity: u64 = location;
        for range in self.humidity_to_location_ranges {
            match range.get_source_value(location) {
                Ok(humidity) => { found_humidity = humidity; break; }
                Err(_) => { }
            }
        }

        let mut found_temperature: u64 = found_humidity;
        for range in self.temperature_to_humidity_ranges {
            match range.get_source_value(found_humidity) {
                Ok(temperature) => { found_temperature = temperature; break; }
                Err(_) => { }
            }
        }

        let mut found_light: u64 = found_temperature;
        for range in self.light_to_temperature_ranges {
            match range.get_source_value(found_temperature) {
                Ok(light) => { found_light = light; break; }
                Err(_) => { }
            }
        }

        let mut found_water: u64 = found_light;
        for range in self.water_to_light_ranges {
            match range.get_source_value(found_light) {
                Ok(water) => { found_water = water; break; }
                Err(_) => { }
            }
        }

        let mut found_fertilizer: u64 = found_water;
        for range in self.fertilizer_to_water_ranges {
            match range.get_source_value(found_water) {
                Ok(fertilizer) => { found_fertilizer = fertilizer; break; }
                Err(_) => { }
            }
        }

        let mut found_soil: u64 = found_fertilizer;
        for range in self.soil_to_fertilizer_ranges {
            match range.get_source_value(found_fertilizer) {
                Ok(soil) => { found_soil = soil; break; }
                Err(_) => { }
            }
        }

        let mut found_seed: u64 = found_soil;
        for range in self.seed_to_soil_ranges {
            match range.get_source_value(found_soil) {
                Ok(seed) => { found_seed = seed; break; }
                Err(_) => { }
            }
        }

        return found_seed;
    }
}

fn part1( lines: Vec<String> ) -> u64 {
    let mut line_index: i32 = 0;

    let mut almanac_ranges: AlmanacRanges = AlmanacRanges {
        seed_to_soil_ranges: vec![],
        soil_to_fertilizer_ranges: vec![],
        fertilizer_to_water_ranges: vec![],
        water_to_light_ranges: vec![],
        light_to_temperature_ranges: vec![],
        temperature_to_humidity_ranges: vec![],
        humidity_to_location_ranges: vec![]
    };

    // Collect seed numbers
    let seed_numbers: Vec<u64> = lines.get(line_index as usize).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();

    // Goto seed-to-soil
    while !lines.get(line_index as usize).unwrap().starts_with("seed-to-soil map:") {
        line_index += 1;
    };
    line_index += 1;

    // Get seed-to-soil ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap().split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();

        almanac_ranges.seed_to_soil_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

     // Get soil_to_fertilizer ranges
     while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.soil_to_fertilizer_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get fertilizer_to_water ranges
     while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.fertilizer_to_water_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get water_to_light ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.water_to_light_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get light_to_temperature ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.light_to_temperature_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get temperature_to_humidity ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.temperature_to_humidity_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get humidity_to_location ranges
    while lines.get(line_index as usize).is_some() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.humidity_to_location_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }

    // Start result at the first seed
    let mut result = almanac_ranges.clone().get_seed_location_value(*seed_numbers.get(0).unwrap());

    // For every seed, iterate over the ranges to find location
    for seed in seed_numbers {
        let seed_location: u64 = almanac_ranges.clone().get_seed_location_value(seed);

        if seed_location < result {
            result = seed_location;
        }
    }

    println!("Part 1 result : {result}");

    return result;
}

fn part2( lines: Vec<String> ) -> u64 {
    let mut line_index: i32 = 0;

    let mut almanac_ranges: AlmanacRanges = AlmanacRanges {
        seed_to_soil_ranges: vec![],
        soil_to_fertilizer_ranges: vec![],
        fertilizer_to_water_ranges: vec![],
        water_to_light_ranges: vec![],
        light_to_temperature_ranges: vec![],
        temperature_to_humidity_ranges: vec![],
        humidity_to_location_ranges: vec![]
    };
    let mut seed_ranges: Vec<ToRange> = vec![];

    // Collect seed ranges
    let seed_input: Vec<u64> = lines.get(line_index as usize).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                    .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
    for seed_range in seed_input.chunks(2) {
        seed_ranges.push(ToRange {
            source: seed_range[0],
            destination : 0,
            length: seed_range[1]
        });
    }

    // Goto seed-to-soil
    while !lines.get(line_index as usize).unwrap().starts_with("seed-to-soil map:") {
        line_index += 1;
    };
    line_index += 1;

    // Get seed-to-soil ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap().split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();

        almanac_ranges.seed_to_soil_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

     // Get soil_to_fertilizer ranges
     while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.soil_to_fertilizer_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get fertilizer_to_water ranges
     while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.fertilizer_to_water_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get water_to_light ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.water_to_light_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get light_to_temperature ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.light_to_temperature_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get temperature_to_humidity ranges
    while !lines.get(line_index as usize).unwrap().is_empty() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.temperature_to_humidity_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }
    line_index += 2;

    // Get humidity_to_location ranges
    while lines.get(line_index as usize).is_some() {
        let range_properties: Vec<u64> = lines.get(line_index as usize).unwrap()
                                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
        almanac_ranges.humidity_to_location_ranges.push( ToRange { 
                                destination: *range_properties.get(0).unwrap(),
                                source: *range_properties.get(1).unwrap(),
                                length: *range_properties.get(2).unwrap() }
                            );
        line_index += 1;
    }

    // Ascending iteration over locations
    for location_iter in 0..u64::MAX {
        let seed: u64 = almanac_ranges.clone().get_location_seed_value(location_iter);

        // Check if found seed exists
        for seed_range in &seed_ranges {
            if let Ok(_) = seed_range.is_contained(seed) {
                let result = location_iter;

                println!("Part 2 result : {result}");
                return result;
            }
        }
    }

    return 0;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    part1(lines.clone());
    part2(lines.clone());
}

#[test]
fn test_part1() {
    let lines: Vec<String> = vec![
        String::from("seeds: 79 14 55 13"),
        String::from(""),
        String::from("seed-to-soil map:"),
        String::from("50 98 2"),
        String::from("52 50 48"),
        String::from(""),
        String::from("soil-to-fertilizer map:"),
        String::from("0 15 37"),
        String::from("37 52 2"),
        String::from("39 0 15"),
        String::from(""),
        String::from("fertilizer-to-water map:"),
        String::from("49 53 8"),
        String::from("0 11 42"),
        String::from("42 0 7"),
        String::from("57 7 4"),
        String::from(""),
        String::from("water-to-light map:"),
        String::from("88 18 7"),
        String::from("18 25 70"),
        String::from(""),
        String::from("light-to-temperature map:"),
        String::from("45 77 23"),
        String::from("81 45 19"),
        String::from("68 64 13"),
        String::from(""),
        String::from("temperature-to-humidity map:"),
        String::from("0 69 1"),
        String::from("1 0 69"),
        String::from(""),
        String::from("humidity-to-location map:"),
        String::from("60 56 37"),
        String::from("56 93 4")
    ];

    assert_eq!(35, part1(lines));
}

#[test]
fn test_part2() {
    let lines: Vec<String> = vec![
        String::from("seeds: 79 14 55 13"),
        String::from(""),
        String::from("seed-to-soil map:"),
        String::from("50 98 2"),
        String::from("52 50 48"),
        String::from(""),
        String::from("soil-to-fertilizer map:"),
        String::from("0 15 37"),
        String::from("37 52 2"),
        String::from("39 0 15"),
        String::from(""),
        String::from("fertilizer-to-water map:"),
        String::from("49 53 8"),
        String::from("0 11 42"),
        String::from("42 0 7"),
        String::from("57 7 4"),
        String::from(""),
        String::from("water-to-light map:"),
        String::from("88 18 7"),
        String::from("18 25 70"),
        String::from(""),
        String::from("light-to-temperature map:"),
        String::from("45 77 23"),
        String::from("81 45 19"),
        String::from("68 64 13"),
        String::from(""),
        String::from("temperature-to-humidity map:"),
        String::from("0 69 1"),
        String::from("1 0 69"),
        String::from(""),
        String::from("humidity-to-location map:"),
        String::from("60 56 37"),
        String::from("56 93 4")
    ];

    assert_eq!(46, part2(lines));
}