use std::env;
use std::fs;

fn is_special_char( character: char ) -> bool {
    if !character.is_digit(10) && character != '.' {
        return true;
    }

    return false;
}

fn extract_number_from_pos( line: &mut Vec<char>, initial_position: usize ) -> i32 {
    let mut running_number_str: String = String::from("");
    let mut position: usize = initial_position;

    // Get every digit : forward
    while line.get(position).unwrap().is_digit(10) {
        running_number_str = format!("{}{}", running_number_str, *line.get(position).unwrap());
        line[position] = 'X'; // Mark position as used to ensure it won't be reused

        position += 1;

        if position > line.len() - 1 {
            break;
        }
    }

    // If start is not at the very first char of the line
    if initial_position > 0 {
        // Rest position
        position = initial_position - 1;

        // Get every digit : backwards
        while line.get(position).unwrap().is_digit(10) {
            running_number_str = format!("{}{}", *line.get(position).unwrap(), running_number_str);
            line[position] = 'X'; // Mark position as used to ensure it won't be reused

            if position == 0 {
                break;
            }

            position -= 1;
        }
    }

    return running_number_str.parse::<i32>().unwrap();
}

fn part1( lines: Vec<String> ) -> i32 {
    let mut result = 0;

    let mut matrix: Vec<Vec<char>> = vec![];
    let mut running_number_str: String = String::from("");
    let mut running_number_is_valid: bool = false;

    // Collect every line characters into a matrix
    for line in lines {
        matrix.push(line.chars().collect::<Vec<char>>());
    }

    // For every line in the matrix
    for ( matrix_line_index, matrix_line ) in matrix.iter().enumerate() {
        // Parse every char in the line
        for ( line_char_index, line_char ) in matrix_line.iter().enumerate() {
            // If char is a digit, add it to running number and check if it's near a special character
            if line_char.is_digit(10) {
                running_number_str.push(*line_char);

                // No need to check if running number is already valid.
                if running_number_is_valid == false {
                    // Checking upper
                    if matrix_line_index > 0 {
                        if is_special_char(*matrix.get(matrix_line_index - 1).unwrap().get(line_char_index).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }

                    // Checking upper left
                    if matrix_line_index > 0 && line_char_index > 0 {
                        if is_special_char(*matrix.get(matrix_line_index - 1).unwrap().get(line_char_index - 1).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }

                    // Checking upper right
                    if matrix_line_index > 0 && line_char_index < matrix_line.len() - 1 {
                        if is_special_char(*matrix.get(matrix_line_index - 1).unwrap().get(line_char_index + 1).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }

                    // Checking left
                    if line_char_index > 0 {
                        if is_special_char(*matrix_line.get(line_char_index - 1).unwrap()){
                            running_number_is_valid = true;
                        }
                    }

                    // Checking right
                    if line_char_index < matrix_line.len() - 1 {
                        if is_special_char(*matrix_line.get(line_char_index + 1).unwrap()){
                            running_number_is_valid = true;
                        }
                    }

                    // Checking bottom left
                    if matrix_line_index < matrix.len() - 1 && line_char_index > 0 {
                        if is_special_char(*matrix.get(matrix_line_index + 1).unwrap().get(line_char_index - 1).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }

                    // Checking bottom
                    if matrix_line_index < matrix.len() - 1 {
                        if is_special_char(*matrix.get(matrix_line_index + 1).unwrap().get(line_char_index).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }

                    // Checking bottom right
                    if matrix_line_index < matrix.len() - 1 && line_char_index < matrix_line.len() - 1 {
                        if is_special_char(*matrix.get(matrix_line_index + 1).unwrap().get(line_char_index + 1).unwrap()) {
                            running_number_is_valid = true;
                        }
                    }
                }

                // Check if it's the last character of the line
                if line_char_index == matrix_line.len() - 1 {
                    // If running number is valid, add it to the result.
                    if running_number_is_valid == true {
                        result += running_number_str.parse::<i32>().unwrap();
                    }
            
                    running_number_str = String::from("");
                    running_number_is_valid = false;
                }
            }
            else{
                // If running number is valid, add it to the result.
                if running_number_is_valid == true {
                    result += running_number_str.parse::<i32>().unwrap();
                }

                running_number_str = String::from("");
                running_number_is_valid = false;
            }
        }
    }

    println!("Part 1 result : {result}");

    return result;
}

fn part2( lines: Vec<String> ) -> i32 {
    let mut result = 0;

    let mut matrix: Vec<Vec<char>> = vec![];
    let mut matrix_mask: Vec<Vec<char>> = vec![];

    // Collect every line characters into a matrix
    for line in lines {
        matrix.push(line.chars().collect::<Vec<char>>());
        matrix_mask.push(line.chars().collect::<Vec<char>>());
    }

    // For every line in the matrix
    for ( matrix_line_index, matrix_line ) in matrix.iter().enumerate() {
        // Parse every char in the line
        for ( line_char_index, line_char ) in matrix_line.iter().enumerate() {
            // If char is a gear ('*'), look after surrounding numbers
            if *line_char == '*' {
                let mut adjacent_numbers: Vec<i32> = vec![];

                // Checking upper
                if matrix_line_index > 0 {
                    if matrix_mask.get(matrix_line_index - 1).unwrap().get(line_char_index).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index-1], line_char_index));
                    }
                }

                // Checking upper left
                if matrix_line_index > 0 && line_char_index > 0 {
                    if matrix_mask.get(matrix_line_index - 1).unwrap().get(line_char_index - 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index-1], line_char_index-1));
                    }
                }

                // Checking upper right
                if matrix_line_index > 0 && line_char_index < matrix_line.len() - 1 {
                    if matrix_mask.get(matrix_line_index - 1).unwrap().get(line_char_index + 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index-1], line_char_index+1));
                    }
                }

                // Checking left
                if line_char_index > 0 {
                    if matrix_mask.get(matrix_line_index).unwrap().get(line_char_index - 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index], line_char_index-1));
                    }
                }

                // Checking right
                if line_char_index < matrix_line.len() - 1 {
                    if matrix_mask.get(matrix_line_index).unwrap().get(line_char_index + 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index], line_char_index+1));
                    }
                }

                // Checking bottom left
                if matrix_line_index < matrix.len() - 1 && line_char_index > 0 {
                    if matrix_mask.get(matrix_line_index + 1).unwrap().get(line_char_index - 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index+1], line_char_index-1));
                    }
                }

                // Checking bottom
                if matrix_line_index < matrix.len() - 1 {
                    if matrix_mask.get(matrix_line_index + 1).unwrap().get(line_char_index).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index+1], line_char_index));
                    }
                }

                // Checking bottom right
                if matrix_line_index < matrix.len() - 1 && line_char_index < matrix_line.len() - 1 {
                    if matrix_mask.get(matrix_line_index + 1).unwrap().get(line_char_index + 1).unwrap().is_digit(10) {
                        adjacent_numbers.push(extract_number_from_pos(&mut matrix_mask[matrix_line_index+1], line_char_index+1));
                    }
                }

                if adjacent_numbers.len() == 2 {
                    result += adjacent_numbers.first().unwrap() * adjacent_numbers.last().unwrap();
                }
            }
        }
    }

    println!("Part 2 result : {result}");

    return result;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    part1(lines.clone());
    part2(lines.clone());
}

#[test]
fn test_part1() {
    let lines: Vec<String> = vec![
        String::from("......2........................"),
        String::from("....&1*22......................"),
        String::from("......3........................"),
        String::from("...........................^..."),
        String::from("...............230..........230"),
        String::from("..............................."),
        String::from("...............................")
    ];

    assert_eq!(258, part1(lines));
}

#[test]
fn test_part2() {
    let lines: Vec<String> = vec![
        String::from("..............................."),
        String::from("....&1*22......................"),
        String::from("..............................."),
        String::from("...........................^..."),
        String::from("...............230..........230"),
        String::from("..............................."),
        String::from("...............................")
    ];

    assert_eq!(22, part2(lines));
}