use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    let mut result = 0;

    // For every line
    for line in lines {
        let mut processed_line = String::from(line);

        // Replace spelled-out digits by their representation.
        // Because spelled-out digits might be mixed together (like "oneight" or "twone"),
        // make sure to leave spelled-out representation before and after
        processed_line = processed_line.replace("one", "one1one");
        processed_line = processed_line.replace("two", "two2two");
        processed_line = processed_line.replace("three", "three3three");
        processed_line = processed_line.replace("four", "four4four");
        processed_line = processed_line.replace("five", "five5five");
        processed_line = processed_line.replace("six", "six6six");
        processed_line = processed_line.replace("seven", "seven7seven");
        processed_line = processed_line.replace("eight", "eight8eight");
        processed_line = processed_line.replace("nine", "nine9nine");

        // Filter every digit in the string.
        let digits: Vec<String> = processed_line.chars()
                            .filter(|c| c.is_digit(10))
                            .map(String::from)
                            .collect();
        
        let mut line_result = String::from(digits.first().unwrap());
        line_result.push_str(digits.last().unwrap());

        result += line_result.parse::<i32>().unwrap();        
    }
    println!("{result}");
}
