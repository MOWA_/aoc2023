use std::env;
use std::fs;

fn part1( lines: Vec<String> ) -> i32 {
    let mut result = 0;

    // For every line
    for line in lines {
        let mut card_result: i32 = 0;
        let winning_numbers: Vec<i32> = line.split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split('|').map(String::from).collect::<Vec<String>>().first().unwrap()
                                .split(' ').flat_map(|x| x.parse::<i32>()).collect::<Vec<i32>>();
        let card_numbers: Vec<i32> = line.split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split('|').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split(' ').flat_map(|x| x.parse::<i32>()).collect::<Vec<i32>>();

        // Compare every winning numbers against card numbers
        for winning_number in winning_numbers.iter() {
            for card_number in card_numbers.iter() {
                // If numbers match, double the result
                if *card_number == *winning_number {
                    if card_result == 0 {
                        card_result = 1;
                    }
                    else {
                        card_result *= 2;
                    }
                }
            }
        }

        result += card_result;
    }

    println!("Part 1 result : {result}");

    return result;
}

fn part2( lines: Vec<String> ) -> i32 {
    let mut result = 0;
    let mut card_copy: [i32; 193] = [1; 193];

    // For every line
    for ( line_index, line ) in lines.iter().enumerate() {
        let winning_numbers: Vec<i32> = line.split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split('|').map(String::from).collect::<Vec<String>>().first().unwrap()
                                .split(' ').flat_map(|x| x.parse::<i32>()).collect::<Vec<i32>>();
        let card_numbers: Vec<i32> = line.split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split('|').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split(' ').flat_map(|x| x.parse::<i32>()).collect::<Vec<i32>>();

        // Each cards counts as one copy
        result += 1;

        for _copy_num in 0..card_copy[line_index] {
            let mut card_result: i32 = 0;

            // Compare every winning numbers against card numbers
            for winning_number in winning_numbers.iter() {
                for card_number in card_numbers.iter() {
                    // If numbers match, increment the result
                    if *card_number == *winning_number {
                        card_result += 1;
                    }
                }
            }

            // Store won card copies
            for copy_num in line_index..line_index + (card_result as usize) {
                card_copy[copy_num+1] += 1;
            }
            
            // Increment result by won copies
            result += card_result;
        }
    }

    println!("Part 2 result : {result}");

    return result;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    part1(lines.clone());
    part2(lines.clone());
}

#[test]
fn test_part1() {
    let lines: Vec<String> = vec![
        String::from("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"),
        String::from("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"),
        String::from("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"),
        String::from("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"),
        String::from("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"),
        String::from("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")
    ];

    assert_eq!(13, part1(lines));
}

#[test]
fn test_part2() {
    let lines: Vec<String> = vec![
        String::from("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"),
        String::from("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"),
        String::from("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"),
        String::from("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"),
        String::from("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"),
        String::from("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")
    ];

    assert_eq!(30, part2(lines));
}