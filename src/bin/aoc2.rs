use std::env;
use std::fs;

fn part1( lines: Vec<String> ) {
    let mut result = 0;

    // For every line
    for line in lines {
        let mut game_is_valid: bool = true;
        let processed_line = String::from(line);

        // Get game id
        let game_id_pattern: String = processed_line.split(':').collect::<Vec<&str>>().first().unwrap().to_string();
        let game_id: i32 = game_id_pattern.split(' ').collect::<Vec<&str>>().last().unwrap().parse::<i32>().unwrap();

        // Get game sets
        let mut game_sets_pattern: String = processed_line.split(':').collect::<Vec<&str>>().last().unwrap().to_string();
        game_sets_pattern = game_sets_pattern.trim().trim_end().to_string();

        // For every set in the game
        for set in game_sets_pattern.split(';').collect::<Vec<&str>>() {
            let processed_set: String = set.trim().trim_end().to_string();

            // For every cubes in the set
            for cubes in processed_set.split(',').collect::<Vec<&str>>() {
                let processed_cubes: String = cubes.trim().trim_end().to_string();

                // Get number and color
                let cubes_number: i32 = processed_cubes.split(' ').collect::<Vec<&str>>().first().unwrap().to_string().parse::<i32>().unwrap();
                let cubes_color: String = processed_cubes.split(' ').collect::<Vec<&str>>().last().unwrap().to_string();
                
                // If found cubes number is greater than the maximum, consider game as invalid
                match cubes_color.as_str() {
                    "red" => {
                        if cubes_number > 12 {
                            game_is_valid = false;
                        }
                    }
                    "green" => {
                        if cubes_number > 13 {
                            game_is_valid = false;
                        }
                    }
                    "blue" => {
                        if cubes_number > 14 {
                            game_is_valid = false;
                        }
                    }
                    _ => panic!("color unknown")
                }
            }
        }

        if game_is_valid {
            result += game_id;
        } 
    }
    println!("Part 1 result : {result}");
}

fn part2( lines: Vec<String> ) {
    let mut result = 0;

    // For every line
    for line in lines {
        let processed_line = String::from(line);

        let mut min_red_cubes: i32 = 0;
        let mut min_green_cubes: i32 = 0;
        let mut min_blue_cubes: i32 = 0;

        // Get game cubes sets
        let mut game_sets_pattern: String = processed_line.split(':').collect::<Vec<&str>>().last().unwrap().to_string();
        game_sets_pattern = game_sets_pattern.trim().trim_end().to_string();

        // For every set in the game
        for set in game_sets_pattern.split(';').collect::<Vec<&str>>() {
            let processed_set: String = set.trim().trim_end().to_string();

            // For every cubes in the set
            for cubes in processed_set.split(',').collect::<Vec<&str>>() {
                let processed_cubes: String = cubes.trim().trim_end().to_string();

                // Get number and color
                let cubes_number: i32 = processed_cubes.split(' ').collect::<Vec<&str>>().first().unwrap().to_string().parse::<i32>().unwrap();
                let cubes_color: String = processed_cubes.split(' ').collect::<Vec<&str>>().last().unwrap().to_string();
                
                // If found number of cubes is greater than the stored one, store it (get the max).
                match cubes_color.as_str() {
                    "red" => {
                        if cubes_number > min_red_cubes {
                            min_red_cubes = cubes_number;
                        }
                    }
                    "green" => {
                        if cubes_number > min_green_cubes {
                            min_green_cubes = cubes_number;
                        }
                    }
                    "blue" => {
                        if cubes_number > min_blue_cubes {
                            min_blue_cubes = cubes_number;
                        }
                    }
                    _ => panic!("color unknown")
                }
            }
        }

        // Sum up !
        result += (min_red_cubes * min_green_cubes * min_blue_cubes);
    }
    println!("Part 2 result : {result}");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    part1(lines.clone());
    part2(lines.clone());
}
