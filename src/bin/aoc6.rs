use std::{env, vec};
use std::fs;

#[derive(Debug, Clone, Copy)]
struct Race {
    time: u64,
    distance: u64
}

impl Race {
    fn get_winning_ways_number( self ) -> u32 {
        let mut winning_ways: u32 = 0;
        for pressed_time in 0..self.time {
            if ( self.time - pressed_time ) * pressed_time > self.distance {
                winning_ways += 1;
            }
        }

        return winning_ways;
    }
}

fn part1( lines: Vec<String> ) -> u32 {
    let mut races: Vec<Race> = vec![];

    // Collect race times
    let race_times: Vec<u64> = lines.get(0).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
    // Collect race distances
    let race_distances: Vec<u64> = lines.get(1).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                                .split(' ').flat_map(|x| x.parse::<u64>()).collect::<Vec<u64>>();
    
    // Build races
    for (race_index, race_time) in race_times.iter().enumerate() {
        races.push(Race { 
            time: *race_time,
            distance: *race_distances.get(race_index).unwrap()
        });
    }

    let mut result = 1;

    // For every race, get the number of ways player can win and multiply it to the result
    for race in races {
        result *= race.get_winning_ways_number();
    }

    println!("Part 1 result : {result}");

    return result;
}

fn part2( lines: Vec<String> ) -> u32 {
    // Collect race time
    let race_time: u64 = lines.get(0).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                            .replace(" ", "").parse::<u64>().unwrap();
    // Collect race distance
    let race_distance: u64 = lines.get(1).unwrap().split(':').map(String::from).collect::<Vec<String>>().last().unwrap()
                            .replace(" ", "").parse::<u64>().unwrap();
    
    // Build race
    let race = Race { 
        time: race_time,
        distance: race_distance
    };

    let result = race.get_winning_ways_number();

    println!("Part 2 result : {result}");

    return result;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path: &String = &args[1];

    println!("Input file {}", file_path);

    /* Get every lines of the file */
    let lines: Vec<String> = fs::read_to_string(file_path)
                        .unwrap()
                        .lines()
                        .map(String::from)
                        .collect();

    part1(lines.clone());
    part2(lines.clone());
}

#[test]
fn test_part1() {
    let lines: Vec<String> = vec![
        String::from("Time:      7  15   30"),
        String::from("Distance:  9  40  200")
    ];

    assert_eq!(288, part1(lines));
}

#[test]
fn test_part2() {
    let lines: Vec<String> = vec![
        String::from("Time:      7  15   30"),
        String::from("Distance:  9  40  200")
    ];

    assert_eq!(71503, part2(lines));
}