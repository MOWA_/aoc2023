# AOC2023

My Rusty solutions to Advent Of Code challenges of 2023. --> https://adventofcode.com

These are not particularly optimal solutions as my goal is to train with Rust code.